let onlineUsers = []

module.exports = {
    addUser: (username) => {
        onlineUsers.push(username)
    },
    readAllUser: () => {
        return onlineUsers
    },
    deleteUser: (username) => {
        onlineUsers = onlineUsers.filter(user => user !== username)
    },
    isUsernameAlreadyExist: (username) => {
        for(let i in onlineUsers){
            if(onlineUsers[i] == username){
                return true
            }
        }
        return false
    }
}