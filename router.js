router = require('express').Router()
user = require('./controllers/userController')

router.get('/api/v1/users/cek/:username', (req,res) => {
    let result = {isAlreadyExist: user.isUsernameAlreadyExist(req.params.username)}
    // console.log(`cek result = ${result}`)
    return res.json(result)
})

router.get('/api/v1/users',(req,res) => {
    let result = user.readAllUser()
    // console.log(`users = ${result}`)
    return res.json({users:result})
})

module.exports = router