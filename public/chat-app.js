// const BASE_URL = 'http://localhost:7000'

const socket = io({ transports: ["websocket"] })

const chatBox = document.getElementById("chat-box")
const chatForm = document.getElementById("chat-form")
const chatMessages = document.getElementById("chat-messages")
const chatInput = document.querySelector("#chat-form>textarea")
const modal = new bootstrap.Modal(document.querySelector(".modal"))
const modalContent = document.getElementById('modal-content')
// const modalBody = document.getElementById('modal-body')
const textWarning = document.getElementById('text-warning')
const username = document.getElementById("username")
const saveUsername = document.getElementById("save-username")
const roomBox = document.getElementById('roomBox')
const usernameText = document.getElementById('username-text')
const usernameTextRight = document.getElementById('username-text-right')
const roomTitle = document.getElementById('room-title')
const rightbar = document.getElementById('rightbar')
let closeCanvas = document.querySelector('[data-bs-dismiss="offcanvas"]');

let yourUsername = "Anonymous"
let roomList = []
let selectedRoom = "public"
let userChatTarget = ""
refreshRoomSelection()

function addChatHistory(chatData) {
    let roomTarget = chatData.from
    let usernamePreview = chatData.from
    let textPreview = chatData.text

    if (chatData.from == yourUsername || chatData.target == 'public') {
        roomTarget = chatData.target
    }
    if (chatData.text.length > 25) {
        textPreview = chatData.text.substring(0, 25) + " ..."
    }

    for (let i in roomList) {
        if (roomList[i].name == roomTarget) {
            roomList[i].chatHistory.push(chatData)

            const lastChatPreview = document.getElementById(`lastChatPreview-${roomTarget}`)
            if (chatData.from == yourUsername) {
                usernamePreview = 'You'
            }
            lastChatPreview.innerHTML = `${usernamePreview} : ${textPreview}`

            increaseUnreadMessage(roomTarget)
            copyRoomboxToRightbar()
            return
        }
    }
    createRoom(roomTarget)
    addChatHistory(chatData)
}

function increaseUnreadMessage(targetRoom) {
    if (targetRoom == selectedRoom) return

    let unreadMessageDivs = Array.from(document.getElementsByClassName('unreadMessage'))
    for (let i in unreadMessageDivs) {
        if (unreadMessageDivs[i].getAttribute('data-id') == targetRoom) {
            if (unreadMessageDivs[i].innerHTML == '999+') return
            // console.log(`[before] unread message room ${targetRoom} = ${unreadMessageDivs[i].innerHTML}`)
            let currentUM = parseInt(unreadMessageDivs[i].innerHTML)
            currentUM++
            if (currentUM > 0) {
                unreadMessageDivs[i].classList.remove('d-none')
                unreadMessageDivs[i].classList.add('d-flex')
                if (currentUM < 100) {
                    unreadMessageDivs[i].style.width = '25px'
                }
                else {
                    unreadMessageDivs[i].style.width = '50px'
                }
            }
            else {
                unreadMessageDivs[i].classList.remove('d-flex')
                unreadMessageDivs[i].classList.add('d-none')
            }

            if (currentUM > 999) {
                currentUM = '999+'
            }

            unreadMessageDivs[i].innerHTML = currentUM.toString()
            // console.log(`unread message room ${targetRoom} = ${unreadMessageDivs[i].innerHTML}`)
            break
        }
    }

}

function createRoom(roomTarget) {
    for (let i in roomList) {
        if (roomList[i].name == roomTarget) return
    }

    roomList.push({ name: roomTarget, chatHistory: [] })
    roomBox.innerHTML += `
    <div id="roomCard" class="room roomCard" data-id="${roomTarget}">
        <div class="d-flex flex-row">
            <h5 class="room text-white w-75" id="roomName" data-id="${roomTarget}"> ${roomTarget}</h5>    
            <div class="w-25 d-flex justify-content-end">
                <div class="unreadMessage bg-primary text-white d-none justify-content-center align-items-center"
                    data-id="${roomTarget}" style="width: 25px;height: 25px;border-radius: 25px;">
                    0
                </div>
            </div>
        </div>
        <p class="room text-white-50 lastChatPreview" id="lastChatPreview-${roomTarget}" data-id="${roomTarget}"> xxxxxxxxxx : xxxxxxxxxxxxxxxxxxxxxxxxx ...</p>
    </div>
    `
}

chatForm.addEventListener("submit", (e) => {
    e.preventDefault()
    if (chatInput.value !== "") {
        let newData = {
            text: chatInput.value,
            target: selectedRoom,
            from: yourUsername,
            date: new Date().toLocaleTimeString(),
        }
        socket.emit("send.chat", newData)
        chatInput.value = ""
    }
})

chatForm.addEventListener("keypress", function (event) {
    if (event.key === "Enter") {
        event.preventDefault()
        document.getElementById("send-button").click()
    }
})

function loadChatInAlreadyOpenedRoom(data) {
    let position = ""
    let color = "bg-secondary"
    if (data.from == yourUsername) {
        position = "float-end"
        color = "bg-primary"
    }

    let newChatBalloon = `
    <div class="card d-inline-block mt-3 ${color} text-white ${position}">
            <div class="card-body">                                
    `

    if (selectedRoom == 'public') {
        if (data.from != yourUsername) {
            newChatBalloon += `<h5 class="card-title">${data.from}</h5>`
        }
    }

    newChatBalloon += `
                <p class="card-text">${data.text}</p>
                <h6 class="card-subtitle mt-2 text-light">${data.date}</h6>
            </div>
        </div>
    <div class="clearfix"></div>
    `

    chatMessages.innerHTML += newChatBalloon
    srollToBottom()
}

function loadRoom() {
    roomTitle.innerHTML = selectedRoom
    chatMessages.innerHTML = ''
    for (let i in roomList) {
        if (roomList[i].name == selectedRoom) {
            for (let x in roomList[i].chatHistory) {
                loadChatInAlreadyOpenedRoom(roomList[i].chatHistory[x])
            }
        }
    }
}

socket.on("connect", () => {
    let newRoom = { name: 'public', chatHistory: [] }
    roomList.push(newRoom)
    modal.show()
})

socket.on("chat.receive", data => {
    if (data.from == yourUsername || data.target == yourUsername || data.target == 'public') {
        addChatHistory(data)
        loadRoom()
    }
})

saveUsername.addEventListener("click", e => {
    if (username.value !== "") {
        if (username.value.length <= 10) {
            fetch(`/api/v1/users/cek/${username.value}`)
                .then(res => res.json())
                .then(res => {
                    // console.log(res.isAlreadyExist)
                    if (res.isAlreadyExist) {
                        textWarning.innerHTML = 'Username already taken by another online user'
                    }
                    else {
                        socket.emit("register", username.value)
                        yourUsername = username.value
                        usernameText.innerHTML = `Your username : ${yourUsername}`
                        usernameTextRight.innerHTML = usernameText.innerHTML
                        modal.hide()
                    }
                })
        }
        else {
            textWarning.innerHTML = 'Username maximum 10 characters !'
        }
    }
    else {
        textWarning.innerHTML = 'Username cannot be empty !'
    }
})

function openUserSelector() {
    console.log('opening user selector')
    modalContent.innerHTML = `
    <div class="modal-header">
        <h5 class="modal-title">Select user to chat with</h5>
    </div>
    <div class="modal-body" id="modal-body">       
        
    </div>
    <div class="modal-footer d-flex flex-column">
        <div>
            <button type="button" id="" class="btn btn-primary" onclick="modalClose()">Back</button>
            <button type="button" id="nextToChat" class="btn btn-primary" onclick="selectUserToChat()">Next</button>
        </div>        
        <p class="text-warning" id="text-warning-us"></p>
    </div>
    `

    fetch(`/api/v1/users`)
        .then(res => res.json())
        .then(res => {
            // console.log(res.users)
            const modalBody = document.getElementById('modal-body')
            let newArr = res.users.filter(user => user !== yourUsername)

            if (newArr.length > 0) {
                for (let i in newArr) {
                    modalBody.innerHTML += `
                        <div class="">
                            <input type="radio" class="" name="userChoices" id="option${i}" value="${newArr[i]}">
                            <label class="form-check-label" for="option${i}">${newArr[i]}</label>
                        </div>
                    `
                }
                document.getElementById('nextToChat').disabled = false
            }
            else {
                document.getElementById('nextToChat').disabled = true
                modalBody.innerHTML = `<p class="text-warning">sorry there are no online users right now</p>`
                // console.log('sorry there are no online users right now')
            }

        })
        .then(modal.show())
}

function selectUserToChat() {
    let userRadioList = Array.from(document.getElementsByName('userChoices'))
    let userRadioListChecked = userRadioList.find(r => r.checked)
    if (userRadioListChecked != null) {
        userChatTarget = userRadioListChecked.value
        selectedRoom = userChatTarget
        createRoom(userChatTarget)
        refreshRoomSelection()
        loadRoom()
        modal.hide()        
        closeCanvas.click()
        // console.log(userChatTarget)        
    }
    else {
        document.getElementById('text-warning-us').innerHTML = 'please select user to chat'
        // console.log('please select user to chat')        
    }
}

function modalClose() {
    modal.hide()
}

function srollToBottom() {
    chatBox.scrollTo(0, chatBox.scrollHeight)
}

roomBox.addEventListener('click', e => {
    if (e.target.classList.contains("room")) {
        e.preventDefault()
        let roomName = e.target.getAttribute("data-id")
        selectedRoom = roomName
        refreshRoomSelection()
        loadRoom()
        closeCanvas.click()
        // console.log(`opening room ${roomName}`)
    }
})

rightbar.addEventListener('click', e => {
    if (e.target.classList.contains("room")) {
        e.preventDefault()
        let roomName = e.target.getAttribute("data-id")
        selectedRoom = roomName
        refreshRoomSelection()
        loadRoom()
        closeCanvas.click()
        // console.log(`opening room ${roomName}`)
    }
})

function refreshRoomSelection() {
    // console.log(`current selected room = ${selectedRoom}`)
    let roomDivs = Array.from(document.getElementsByClassName('roomCard'))
    for (let i in roomDivs) {
        roomDivs[i].style.removeProperty('background-color')
        if (roomDivs[i].getAttribute('data-id') == selectedRoom) {
            roomDivs[i].style.backgroundColor = '#808080'
        }
    }

    let unreadMessageDivs = Array.from(document.getElementsByClassName('unreadMessage'))
    for (let i in unreadMessageDivs) {
        if (unreadMessageDivs[i].getAttribute('data-id') == selectedRoom) {
            // console.log(`change unread message for room ${selectedRoom} to zero !`)
            unreadMessageDivs[i].innerHTML = '0'
            unreadMessageDivs[i].classList.remove('d-flex')
            unreadMessageDivs[i].classList.add('d-none')
        }
    }
    copyRoomboxToRightbar()
}

function copyRoomboxToRightbar() {
    rightbar.innerHTML = roomBox.innerHTML
}

