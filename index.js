//server ================================
const express = require("express")
const app = express()
const http = require("http")
const server = http.createServer(app)
const SocketIO = require("socket.io").Server
const io = new SocketIO(server)
const router = require('./router')
//controllers ============================
const user = require('./controllers/userController')

//middware ===============================
app.use("/", express.static("public"))
app.use(router)

//socketio server side ===================
io.on("connection", (socket) => {
    console.log(`new socket io connected: ${socket.id}`)

    //register new online user
    let thisUser = "Anonymous"
    socket.on("register", (data) => {       
        thisUser = data
        user.addUser(data)
    })

    // chat event...
    socket.on("send.chat", chatData => {        
        // console.log(chatData)
        socket.broadcast.emit("chat.receive", chatData)
        socket.emit("chat.receive", chatData)
    })

    socket.on("disconnect", () => {
        user.deleteUser(thisUser)        
        console.log(`disconnected client: ${socket.id}`)
    })
})

server.listen(7000, () => {
    console.log(`server running on port 7000`)
})
